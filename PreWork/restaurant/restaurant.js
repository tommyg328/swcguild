// *****FORM VALIDATION*****

function validateForm() {
	var nameCheck = document.forms["myForm"]["name"].value;
	if (nameCheck == null || nameCheck == "") {
		alert("Name must be filled out");
		return false;
	}

	var emailCheck = document.forms["myForm"]["email"].value;
	if (emailCheck == null || emailCheck == "") {
		alert("Email must be filled out");
		return false;
	}

	var reason = document.forms["myForm"]["reason"].value;
	var text = document.forms["myForm"]["text"].value;
	if (reason == "other" && text == "") {
		alert("Please fill in the 'Additionl Information' box");
		return false;
	}

	var checked1 = document.myForm.checkbox1.checked;
	var checked2 = document.myForm.checkbox2.checked;
	var checked3 = document.myForm.checkbox3.checked;
	var checked4 = document.myForm.checkbox4.checked;
	var checked5 = document.myForm.checkbox5.checked;

	if (checked1 == false &&
		checked2 == false &&
		checked3 == false &&
		checked4 == false &&
		checked5 == false) {

		alert("Please let us know which day(s) are best to contact you.\nCheck at least one box.");
		return false;
	}
}


