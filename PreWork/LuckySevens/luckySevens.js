
	alert("Welcome to Lucky Sevens! \nPlease enter a starting bet between $1 and $1000");

	function hideResults(){
		 document.getElementById("resultsHide").style.display = "none";
		
	}
	// WAGER & DICE ROLL	
	function playGame() {

		var startingBet = document.getElementById("userBet").value;
		var money = startingBet;
		var myArray = []

		while (money > 0) {
			var diceOne = Math.floor(Math.random() * 6) + 1;
			var diceTwo = Math.floor(Math.random() * 6) + 1;
			var totalDice = diceOne + diceTwo;

			if (totalDice === 7) {
				money += 4;
				console.log("Winner");
			} else {
				money -= 1;
			}
			myArray.push(money);
			console.log(totalDice);
		}

	//RESULTS TABLE
		
		var totalRolls = myArray.length;
		var highestWon = Math.max.apply(Math, myArray);
		var highestWonRolls = totalRolls - highestWon;

		function showResults() {
			document.getElementById("resultsHide").style.display = "inline";
			document.getElementById("btn").innerHTML = "Play Again";
			document.getElementById("resultsUserBet").innerHTML = "$" + startingBet + ".00";
			document.getElementById("totalRollsB4Broke").innerHTML = totalRolls;
			document.getElementById("highestWon").innerHTML = "$" + highestWon + ".00";
			document.getElementById("rollsHighestWon").innerHTML = highestWonRolls;
		};

		showResults();
	}
